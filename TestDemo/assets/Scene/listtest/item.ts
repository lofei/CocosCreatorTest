const {ccclass, property} = cc._decorator;

@ccclass
export default class item extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    private _removeCall:Function = null;
    private _index: number = 0;

    init(data: any) {
        this.label.string = data.text;
        this._removeCall = data.removeCall;
        this._index = data.index;
    }

    onClickRemove() {
        this._removeCall && this._removeCall(this._index);
    }

}
