const {ccclass, property} = cc._decorator;

@ccclass
export default class scrollViewExecuteOreFrame extends cc.Component {

    private executePreFrame(generator: Generator, duration: number) {
        return new Promise((resolve, reject) => {
            let gen = generator;
            let execute = () => {
                let startTime = new  Date().getTime();
                for (let iter = gen.next(); ; iter = gen.next()) {
                    if (iter == null || iter.done) {
                        resolve();
                        return;
                    }

                    if (new Date().getTime() - startTime > duration) {
                        this.scheduleOnce(() => {
                            execute();
                        });
                        return;
                    }
                }
            };
            execute();
        });
    }
}
