import ListScrollView from "./ListScrollView";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property
    initCount: number = 0;
    @property(ListScrollView)
    scroll: ListScrollView = null;

    private _data: any[] = [];

    onLoad () {
        this.initData();
    }

    start () {
        this.scroll.init({
            getListData: () => {
                return this._data.map((d, index) => {
                    return {
                        index: index,
                        text: `# item ${d}`,
                        removeCall: this.removeItem.bind(this),
                    }
                });
            }
        });
        this.scroll.reload();
    }

    // update (dt) {}

    initData() {
        this._data = [];
        while(this._data.length < this.initCount) {
            this._data.push(this._data.length);
        }
    }

    removeItem(index: number) {
        this._data.splice(index, 1);
        this.scroll.reload(true);
    }

    onClickReload() {
        this.initData();
        this.scroll.reload();
    }
}
