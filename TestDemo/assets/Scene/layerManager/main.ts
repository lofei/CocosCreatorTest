import EventManager from "./eventManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Main extends cc.Component {
    onLoad() {
    }

    start() {
        setInterval(() => {
            EventManager.pushEvent({'msgId': 'OPEN_LAYER', 'layerId' : 'testLayer', 'layerParent': this.node});
        }, 100);
    }

    update() {
        const str = this.node.children.reduce((s, node) => {
            s += node.name + ', '
            return s;
        }, '')
        cc.log(str)
    }
}
