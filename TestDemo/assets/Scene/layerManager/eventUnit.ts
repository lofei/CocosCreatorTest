import EventManager from "./eventManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class EventUnit extends cc.Component {
    update(dt: number) {
        EventManager.processEvent();
    }
}
