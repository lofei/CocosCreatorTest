// luofei
//

import { Typewriter } from "./TestSceneScripts/Typewriter";
import { ChatDialog } from "./TestSceneScripts/ChatDialog";
import { DragonBonesAnimation } from "./TestSceneScripts/DragonBonesAnimation";
import { Tiledmap } from "./TestSceneScripts/TiledmapScene";
import { PerlinNoise } from "./TestSceneScripts/PerlinNoise";
import { BehaviorTreeTest } from "./TestSceneScripts/BehaviorTreeTest";
import { Light } from "./TestSceneScripts/Light";
import { SightAndLight } from "./TestSceneScripts/SightAndLight";
import { WalkingRoute } from './TestSceneScripts/WalkingRoute';
import { ListTest } from "./TestSceneScripts/ListView/ListTest";

// 测试项
export class TestItem {
    // 测试项目组件
    private mComponent: null;
    // 项目脚本名称
    private mScript: string;
    // 项目显示名称
    private mShowText: string;

    constructor(component, script: string, showText: string) {
        this.mComponent = component;
        this.mScript = script;
        this.mShowText = showText;
    }

    public get component() {
        return this.mComponent;
    }

    public get script() {
        return this.mScript;
    }

    public set script(script: string) {
        this.mScript = script;
    }

    public get showText() {
        return this.mShowText;
    }

    public set showText(showText: string) {
        this.mShowText = showText;
    }
}

const { ccclass, property } = cc._decorator;

// 所有测试脚本管理
@ccclass
export class TestItemManager {
    private _testItems = [];

    constructor() {
        this.registerAllTestClass();
    }

    private registerAllTestClass() {
        this._testItems.push(new TestItem(Typewriter, "Typewriter", "打字机动画"));
        this._testItems.push(new TestItem(ChatDialog, "ChatDialog", "Tab标签页"));
        this._testItems.push(new TestItem(DragonBonesAnimation, "DragonBonesAnimation", "DragonBones动画"));
        this._testItems.push(new TestItem(Tiledmap, "Tiledmap", "Tiledmap地图"));
        this._testItems.push(new TestItem(PerlinNoise, "PerlinNoise", "柏林噪声"));
        this._testItems.push(new TestItem(BehaviorTreeTest, "BehaviorTreeTest", "行为树测试"));
        this._testItems.push(new TestItem(Light, "Light", "拖尾效果测试"));
        this._testItems.push(new TestItem(SightAndLight, "SightAndLight", "光影特效"));
        this._testItems.push(new TestItem(WalkingRoute, 'WalkingRoute', '路线移动'));
        this._testItems.push(new TestItem(ListTest, 'ListTest', '无限列表'));
    }

    public getTestItems() {
        return this._testItems;
    }
}
