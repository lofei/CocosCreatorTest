// luofei
//

const { ccclass, property } = cc._decorator;

// 测试菜单项
@ccclass
export class TestItemWidget extends cc.Component {

    // 项目在菜单中显示信息
    @property(cc.Label)
    private mItemName: cc.Label = null;

    // 测试项目脚本名称
    private _itemName: string = "";
    // 测试项触摸事件
    private _touchCallback = null;
    // 测试项目索引
    private _itemIndex: number = 0;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }

    // update (dt) {}

    // 设置显示信息
    set itemName(itemName: string) {
        this._itemName = itemName;
        this.mItemName.string = this._itemName;
        cc.log("创建: " + itemName);
    }

    // 设置触摸回调
    set touchCallback(callback) {
        this._touchCallback = callback;
    }

    // 设置项目索引
    set itemIndex(index: number) {
        this._itemIndex = index;
    }

    // 进入测试场景
    intoTestScene(){
        if (!this._touchCallback) {
            cc.log(this._itemName + " 未设置触摸回调");
            return;
        }

        this._touchCallback();
    }

}
