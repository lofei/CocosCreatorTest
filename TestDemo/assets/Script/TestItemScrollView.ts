// luofei
//

import { TestItem, TestItemManager } from "./TestItemManager";
import { TestItemWidget } from "./TestItemWidget";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TestItemScrollView extends cc.Component {

    // 列表显示内容
    @property(cc.Node)
    mContent: cc.Node = null;

    // 模板
    @property(cc.Node)
    mItemTemplate: cc.Node = null;

    // 所有项目
    _items: cc.Node[] = [];
    // 加载的所有项
    _loadItems: TestItem[] = [];
    // 实际项总数
    _itemsCount: number = 0;
    // 显示项总数
    _showCount: number = 0;
    // 间隔
    _spacing: number = 5;
    // 资源载入状态
    _nowLoadIndex: number = 0;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.mItemTemplate.active = false;
        this.initTestItems();
    }

    start() {

    }

    // update (dt) {}

    // 加载所有测试列表
    private initTestItems(): void {
        this.mContent.removeAllChildren();
        this._items = [];

        this._loadItems = [];
        let manager = new TestItemManager();
        this._loadItems = manager.getTestItems();

        let item: TestItem = this._loadItems[0];
        this.addToItems(item.script, item.showText);
    }

    // 添加测试项
    private addToItems(scriptName: string, showText: string): void {
        let self = this;
        let prefabPath = "prefab/TestItemWidget";

        let onResourceLoaded = function (errorMessage: Error, loadedResource: cc.Prefab) {
            if (errorMessage) {
                cc.log("载入失败：" + errorMessage);
                return;
            }

            if (!loadedResource) {
                cc.log("载入资源获取失败");
                return;
            }

            let item = cc.instantiate(loadedResource);
            self.mContent.addChild(item);

            // 隔行变色
            if (self._items.length % 2 == 0) {
                item.color = new cc.Color(0, 0, 0);
                item.children[0].color = new cc.Color(255, 255, 255);
            } else {
                item.color = new cc.Color(255, 255, 255);
                item.children[0].color = new cc.Color(0, 0, 0);
            }

            let script: TestItemWidget = item.getComponent("TestItemWidget");
            script.itemName = showText;
            script.itemIndex = self._items.length;
            script.touchCallback = function () {
                cc.director.loadScene(scriptName);
            }

            item.on(cc.Node.EventType.TOUCH_END, function (event) {
                cc.log("进入测试场景：" + scriptName);
                cc.director.loadScene(scriptName + "Scene");
                cc.director.preloadScene("TestItemScrollView");
            }, this);

            self._items.push(item);
            self._nowLoadIndex++;
            cc.log(scriptName + " 已加入， 长度：" + self._items.length);

            // 保证每次打开菜单顺序一直，这里在当前资源加载完之后再去加载下一个
            if (self._loadItems[self._nowLoadIndex]) {
                let nextItem = self._loadItems[self._nowLoadIndex]
                self.addToItems(nextItem.script, nextItem.showText);
            }

            // 全部加载完后重置尺寸
            if (self._nowLoadIndex == self._loadItems.length) {
                self.resizeContentSize();
            }
        }


        cc.loader.loadRes(prefabPath, onResourceLoaded);
    }

    // 重置列表尺寸
    resizeContentSize() {
        // 每项尺寸
        var itemSize = cc.size(625, 80);
        // 行数 
        this.mContent.height = this._items.length * (itemSize.height + this._spacing) + this._spacing;
        for (var i = 0; i < this._items.length; i++) {
            var item = this._items[i];
            item.setPosition(0, -item.height * (0.5 + i) - this._spacing * (i + 1) - 10);
        }
    }
}
