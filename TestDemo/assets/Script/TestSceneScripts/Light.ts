// lofei
//
// 拖尾效果

const { ccclass, property } = cc._decorator;

@ccclass
export class Light extends cc.Component {

    @property({type:cc.Node, displayName:"图片"})
    mImg: cc.Node = null;

    @property({displayName:"响应速度"})
    mSpeed:number = 0;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
    }

    start() {
        let self = this;
        this.schedule(function(){
            let x = Math.floor(Math.random() * (1000 - 100 + 1) + 100)
            let y = Math.floor(Math.random() * (550 - 100 + 1) + 100)
            self.moveImgToTouchPos(cc.v2(x, y));
        }, 0.2 + this.mSpeed)
    }

    moveImgToTouchPos (pos:cc.Vec2) {
        this.mImg.position = pos;
    }

    // update (dt) {}    
    backToHome() {
        cc.director.loadScene("TestItemScrollView");
    }
}
