// luofei
//

const { ccclass, property } = cc._decorator;

// 
@ccclass
export class PerlinNoise extends cc.Component {

    // LIFE-CYCLE CALLBACKS:
    @property({ displayName: "振幅", type: cc.EditBox })
    mAmplitude: cc.EditBox = null;

    @property({ displayName: "频率", type: cc.EditBox })
    mFrequency: cc.EditBox = null;

    @property({ displayName: "绘制", type: cc.Graphics })
    mDraw: cc.Graphics = null;

    // onLoad () {},

    start() {
        this.mAmplitude.string = "10";
        this.mFrequency.string = "10";
    }

    // update (dt) {},

    startDraw() {
        if (isNaN(parseInt(this.mAmplitude.string))) {
            cc.log("振幅必须为整数");
            return;
        }

        let amplitude = parseInt(this.mAmplitude.string);

        if (isNaN(parseInt(this.mFrequency.string))) {
            cc.log("频率必须为整数");
            return;
        }

        let frequency = parseInt(this.mFrequency.string);

        this.mDraw.clear();
        let col = cc.Color.RED;
        col.setA(255);
        this.mDraw.strokeColor = col;

        this.mDraw.moveTo(0, 0);

        for (let i = 0; i < amplitude; i++) {
            let randomNum = this.getNoise(frequency + i);

            let pos = cc.v2(i * 10 , randomNum * 50);
            this.mDraw.lineTo(pos.x, pos.y);

            cc.log("draw: " + pos.x + " : " + pos.y);
        }
        this.mDraw.stroke();
    }

    getNoise(num: number) {
        let n = (num << 13) ^ num;
        return (1.0 - ((num * (num * num * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
    }

    getLinearInterpolation(min: number, max: number, offset: number) {
        return min * (1 - offset) + max * offset;
    }

    backToHome() {
        cc.director.loadScene("TestItemScrollView");
    }
}
