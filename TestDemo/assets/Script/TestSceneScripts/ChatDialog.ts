// luofei
//

const { ccclass, property } = cc._decorator;

// 
@ccclass
export class ChatDialog extends cc.Component {

    // tab按钮
    @property(cc.Node)
    mTabButtons: cc.Node = null;
    // tab内容
    @property(cc.Node)
    mTabPanels: cc.Node = null;
    @property([cc.Button])
    _mTabButtons: cc.Button[] = [];
    @property([cc.Node])
    _mTabPanels: cc.Node[] = [];

    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        this.initChatDialog();
    }

    start() {

    }

    // update (dt) {},

    // 初始化聊天控件
    initChatDialog() {
        let allTabButtons = [];
        let allTabPanels = [];
        for (let i = 0; i < this.mTabButtons.children.length; i++) {
            allTabButtons.push(this.mTabButtons.children[i]);
            allTabPanels.push(this.mTabPanels.children[i]);
        }
        let self = this;

        if (allTabButtons.length > 0 && allTabPanels.length > 0) {
            for (let i = 0; i < allTabButtons.length; i++) {
                this._mTabButtons.push(allTabButtons[i]);

                let tabPanel = allTabPanels[i];
                tabPanel.active = false;
                this._mTabPanels.push(tabPanel);
            }
        }
    }

    showPanel(event, customData) {
        for (let i = 0; i < this._mTabPanels.length; i++) {
            this._mTabPanels[i].active = this._mTabPanels[i].name == customData;
            cc.log("显示tabPanel " + i + " : " + (this._mTabPanels[i] == customData));
        }
    }

    backToHome() {
        cc.director.loadScene("TestItemScrollView");
    }
}
