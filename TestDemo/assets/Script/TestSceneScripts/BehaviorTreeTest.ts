// lofei
//
// 行为树测试

const { ccclass, property } = cc._decorator;

@ccclass
export class BehaviorTreeTest extends cc.Component {

    @property({ type: cc.Node, displayName: "角色节点" })
    mRobot: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }

    // update (dt) {}
    backToHome() {
        cc.director.loadScene("TestItemScrollView");
    }
}
