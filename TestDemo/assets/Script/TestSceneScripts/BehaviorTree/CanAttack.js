cc.Class({
    extends: cc.Component,

    properties: {

    },


    onLoad: function () {

    },
    enter: function (tick, b3, treeNode) {

    },
    open: function (tick, b3, treeNode) {

    },
    tick: function (tick, b3, treeNode) {
        // cc.log("canAttack");
        if (this.getComponent("Robot").mHP >= this.getComponent("Robot").mHPConsume && this.getComponent("Robot").mMP >= this.getComponent("Robot").mMPConsume) {
            return b3.SUCCESS;
        } else {
            return b3.FAILURE;
        }
    },
    close: function (tick, b3, treeNode) {

    },
    exit: function (tick, b3, treeNode) {

    },

});
