//Don't modify this if you want to re-modify the behaviortree in the future
//#########################################{"class":"go.TreeModel","nodeDataArray":[{"catagory":"Composite","color":"lightgreen","key":-1,"loc":"191.1446313263512 399.198426060569","name":"Root","parameter":"{}","src":"icon/root.svg","textEditable":false,"type":"Root"},{"catagory":"Action","color":"lightcoral","key":-2,"loc":"509.8437100220049 429.7498717765289","name":"Sleep","parameter":"{}","parent":-12,"src":"icon/customaction.svg","textEditable":true,"type":"Action"},{"catagory":"Condition","color":"palegoldenrod","key":-3,"loc":"526.9851783647318 165.60502643602496","name":"CheckHP","parameter":"{\"hp\":100}","parent":-11,"src":"icon/customcondition.svg","textEditable":true,"type":"Condition"},{"catagory":"Condition","color":"palegoldenrod","key":-4,"loc":"510.8967047353843 376.28910776459503","name":"CheckMP","parameter":"{\"mp\":100}","parent":-12,"src":"icon/customcondition.svg","textEditable":true,"type":"Condition"},{"catagory":"Action","color":"lightcoral","key":-5,"loc":"533.1824190210982 223.62244109792852","name":"Eat","parameter":"{}","parent":-11,"src":"icon/customaction.svg","textEditable":true,"type":"Action"},{"catagory":"Condition","color":"palegoldenrod","key":-6,"loc":"450.9443237830031 521.7652982407853","name":"CanAttack","parameter":"{}","parent":-13,"src":"icon/customcondition.svg","textEditable":true,"type":"Condition"},{"catagory":"Action","color":"lightcoral","key":-7,"loc":"445.8967047353844 578.4795839550712","name":"Attack","parameter":"{}","parent":-13,"src":"icon/customaction.svg","textEditable":true,"type":"Action"},{"catagory":"Action","color":"lightcoral","key":-20,"loc":"535.9693876871547 276.5292958164099","name":"Wait","parameter":"{\"milliseconds\":3000}","parent":-11,"src":"icon/wait.svg","textEditable":true,"type":"Wait"},{"catagory":"Action","color":"lightcoral","key":-9,"loc":"504.4846938435774 471.26452583789245","name":"Wait","parameter":"{\"milliseconds\":3000}","parent":-12,"src":"icon/wait.svg","textEditable":true,"type":"Wait"},{"catagory":"Action","color":"lightcoral","key":-10,"loc":"437.4846938435774 638.2645258378926","name":"Wait","parameter":"{\"milliseconds\":3000}","parent":-13,"src":"icon/wait.svg","textEditable":true,"type":"Wait"},{"catagory":"Composite","color":"lightblue","key":-11,"loc":"391.4846938435775 285.2645258378925","name":"Sequence","parameter":"{}","parent":-14,"src":"icon/sequence.svg","textEditable":true,"type":"Sequence"},{"catagory":"Composite","color":"lightblue","key":-12,"loc":"384.4846938435775 407.26452583789245","name":"Sequence","parameter":"{}","parent":-14,"src":"icon/sequence.svg","textEditable":true,"type":"Sequence"},{"catagory":"Composite","color":"lightblue","key":-13,"loc":"339.4846938435775 503.26452583789256","name":"Sequence","parameter":"{}","parent":-14,"src":"icon/sequence.svg","textEditable":true,"type":"Sequence"},{"catagory":"Composite","color":"lightblue","key":-14,"loc":"262.48469384357753 405.2645258378925","name":"Priority","parameter":"{}","parent":-1,"src":"icon/priority.svg","textEditable":true,"type":"Priority"}]}#############################################


cc.Class({
extends: cc.Component,
editor: {
inspector: 'packages://behaviortree-editor/bt-inspector.js'
},
properties: {
},
onLoad: function () {
let b3 = require('b3core.0.1.0module');
let self = this;
let  Sleep = b3.Class(b3.Action);
Sleep.prototype.name = 'Sleep';
Sleep.prototype.__Action_initialize = Sleep.prototype.initialize;
Sleep.prototype.initialize = function(settings){
         settings = settings || {};
         this.__Action_initialize();
         this.parameter = settings.parameter;
}
Sleep.prototype.enter = function(tick){
           return self.getComponent('Sleep').enter(tick,b3,this);
}
Sleep.prototype.open = function(tick) {
           return self.getComponent('Sleep').open(tick,b3,this);
}
Sleep.prototype.tick = function(tick) {
           return self.getComponent('Sleep').tick(tick,b3,this);
}
Sleep.prototype.close = function(tick) {
           return self.getComponent('Sleep').close(tick,b3,this);
}
Sleep.prototype.exit = function(tick) {
           return self.getComponent('Sleep').exit(tick,b3,this);
}
let  CheckHP = b3.Class(b3.Condition);
CheckHP.prototype.name = 'CheckHP';
CheckHP.prototype.__Condition_initialize = CheckHP.prototype.initialize;
CheckHP.prototype.initialize = function(settings){
         settings = settings || {};
         this.__Condition_initialize();
         this.parameter = settings.parameter;
}
CheckHP.prototype.enter = function(tick){
           return self.getComponent('CheckHP').enter(tick,b3,this);
}
CheckHP.prototype.open = function(tick) {
           return self.getComponent('CheckHP').open(tick,b3,this);
}
CheckHP.prototype.tick = function(tick) {
           return self.getComponent('CheckHP').tick(tick,b3,this);
}
CheckHP.prototype.close = function(tick) {
           return self.getComponent('CheckHP').close(tick,b3,this);
}
CheckHP.prototype.exit = function(tick) {
           return self.getComponent('CheckHP').exit(tick,b3,this);
}
let  CheckMP = b3.Class(b3.Condition);
CheckMP.prototype.name = 'CheckMP';
CheckMP.prototype.__Condition_initialize = CheckMP.prototype.initialize;
CheckMP.prototype.initialize = function(settings){
         settings = settings || {};
         this.__Condition_initialize();
         this.parameter = settings.parameter;
}
CheckMP.prototype.enter = function(tick){
           return self.getComponent('CheckMP').enter(tick,b3,this);
}
CheckMP.prototype.open = function(tick) {
           return self.getComponent('CheckMP').open(tick,b3,this);
}
CheckMP.prototype.tick = function(tick) {
           return self.getComponent('CheckMP').tick(tick,b3,this);
}
CheckMP.prototype.close = function(tick) {
           return self.getComponent('CheckMP').close(tick,b3,this);
}
CheckMP.prototype.exit = function(tick) {
           return self.getComponent('CheckMP').exit(tick,b3,this);
}
let  Eat = b3.Class(b3.Action);
Eat.prototype.name = 'Eat';
Eat.prototype.__Action_initialize = Eat.prototype.initialize;
Eat.prototype.initialize = function(settings){
         settings = settings || {};
         this.__Action_initialize();
         this.parameter = settings.parameter;
}
Eat.prototype.enter = function(tick){
           return self.getComponent('Eat').enter(tick,b3,this);
}
Eat.prototype.open = function(tick) {
           return self.getComponent('Eat').open(tick,b3,this);
}
Eat.prototype.tick = function(tick) {
           return self.getComponent('Eat').tick(tick,b3,this);
}
Eat.prototype.close = function(tick) {
           return self.getComponent('Eat').close(tick,b3,this);
}
Eat.prototype.exit = function(tick) {
           return self.getComponent('Eat').exit(tick,b3,this);
}
let  CanAttack = b3.Class(b3.Condition);
CanAttack.prototype.name = 'CanAttack';
CanAttack.prototype.__Condition_initialize = CanAttack.prototype.initialize;
CanAttack.prototype.initialize = function(settings){
         settings = settings || {};
         this.__Condition_initialize();
         this.parameter = settings.parameter;
}
CanAttack.prototype.enter = function(tick){
           return self.getComponent('CanAttack').enter(tick,b3,this);
}
CanAttack.prototype.open = function(tick) {
           return self.getComponent('CanAttack').open(tick,b3,this);
}
CanAttack.prototype.tick = function(tick) {
           return self.getComponent('CanAttack').tick(tick,b3,this);
}
CanAttack.prototype.close = function(tick) {
           return self.getComponent('CanAttack').close(tick,b3,this);
}
CanAttack.prototype.exit = function(tick) {
           return self.getComponent('CanAttack').exit(tick,b3,this);
}
let  Attack = b3.Class(b3.Action);
Attack.prototype.name = 'Attack';
Attack.prototype.__Action_initialize = Attack.prototype.initialize;
Attack.prototype.initialize = function(settings){
         settings = settings || {};
         this.__Action_initialize();
         this.parameter = settings.parameter;
}
Attack.prototype.enter = function(tick){
           return self.getComponent('Attack').enter(tick,b3,this);
}
Attack.prototype.open = function(tick) {
           return self.getComponent('Attack').open(tick,b3,this);
}
Attack.prototype.tick = function(tick) {
           return self.getComponent('Attack').tick(tick,b3,this);
}
Attack.prototype.close = function(tick) {
           return self.getComponent('Attack').close(tick,b3,this);
}
Attack.prototype.exit = function(tick) {
           return self.getComponent('Attack').exit(tick,b3,this);
}
let tree = new b3.BehaviorTree();
tree.root = new b3.Sequence({parameter:{},children:[new b3.Priority({parameter:{},children:[new b3.Sequence({parameter:{},children:[new CheckHP({parameter:{'hp':100}}),new Eat({parameter:{}}),new b3.Wait({parameter:{'milliseconds':3000},milliseconds:3000})]}),new b3.Sequence({parameter:{},children:[new CheckMP({parameter:{'mp':100}}),new Sleep({parameter:{}}),new b3.Wait({parameter:{'milliseconds':3000},milliseconds:3000})]}),new b3.Sequence({parameter:{},children:[new CanAttack({parameter:{}}),new Attack({parameter:{}}),new b3.Wait({parameter:{'milliseconds':3000},milliseconds:3000})]})]})]});
this.tree = tree;
this.blackboard = new b3.Blackboard();
this.b3 = b3;
},
tick: function(target){
let t = {};
if(target != undefined){t = target;}
this.tree.tick(t,this.blackboard)
}});
