//Don't modify this if you want to re-modify the behaviortree in the future
//#########################################{"class":"go.TreeModel","nodeDataArray":[{"catagory":"Composite","color":"lightgreen","key":-1,"loc":"18.729980468749993 118.45641601562475","name":"Root","parameter":"{}","src":"icon/root.svg","textEditable":false,"type":"Root"},{"catagory":"Action","color":"lightcoral","key":-2,"loc":"151.0299804687492 197.8364160156246","name":"Attack","parameter":"{}","parent":-1,"src":"icon/customaction.svg","textEditable":true,"type":"Action"},{"catagory":"Condition","color":"palegoldenrod","key":-3,"loc":"143.3124804687496 22.538916015625002","name":"LookAround","parameter":"{\"radio\":10}","parent":-1,"src":"icon/customcondition.svg","textEditable":true,"type":"Condition"}]}#############################################


cc.Class({
extends: cc.Component,
editor: {
inspector: 'packages://behaviortree-editor/bt-inspector.js'
},
properties: {
},
onLoad: function () {
let b3 = require('b3core.0.1.0module');
let self = this;
let  Attack = b3.Class(b3.Action);
Attack.prototype.name = 'Attack';
Attack.prototype.__Action_initialize = Attack.prototype.initialize;
Attack.prototype.initialize = function(settings){
         settings = settings || {};
         this.__Action_initialize();
         this.parameter = settings.parameter;
}
Attack.prototype.enter = function(tick){
           return self.getComponent('Attack').enter(tick,b3,this);
}
Attack.prototype.open = function(tick) {
           return self.getComponent('Attack').open(tick,b3,this);
}
Attack.prototype.tick = function(tick) {
           return self.getComponent('Attack').tick(tick,b3,this);
}
Attack.prototype.close = function(tick) {
           return self.getComponent('Attack').close(tick,b3,this);
}
Attack.prototype.exit = function(tick) {
           return self.getComponent('Attack').exit(tick,b3,this);
}
let  LookAround = b3.Class(b3.Condition);
LookAround.prototype.name = 'LookAround';
LookAround.prototype.__Condition_initialize = LookAround.prototype.initialize;
LookAround.prototype.initialize = function(settings){
         settings = settings || {};
         this.__Condition_initialize();
         this.parameter = settings.parameter;
}
LookAround.prototype.enter = function(tick){
           return self.getComponent('LookAround').enter(tick,b3,this);
}
LookAround.prototype.open = function(tick) {
           return self.getComponent('LookAround').open(tick,b3,this);
}
LookAround.prototype.tick = function(tick) {
           return self.getComponent('LookAround').tick(tick,b3,this);
}
LookAround.prototype.close = function(tick) {
           return self.getComponent('LookAround').close(tick,b3,this);
}
LookAround.prototype.exit = function(tick) {
           return self.getComponent('LookAround').exit(tick,b3,this);
}
let tree = new b3.BehaviorTree();
tree.root = new b3.Sequence({parameter:{},children:[new LookAround({parameter:{'radio':10}}),new Attack({parameter:{}})]});
this.tree = tree;
this.blackboard = new b3.Blackboard();
this.b3 = b3;
},
tick: function(target){
let t = {};
if(target != undefined){t = target;}
this.tree.tick(t,this.blackboard)
}});
