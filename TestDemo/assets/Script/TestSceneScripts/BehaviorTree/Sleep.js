cc.Class({
    extends: cc.Component,

    properties: {

    },


    onLoad: function () {

    },
    enter: function (tick, b3, treeNode) {

    },
    open: function (tick, b3, treeNode) {

    },
    tick: function (tick, b3, treeNode) {
        // cc.log("toSleep");
        if (this.getComponent("Robot").toSleep()) {
            return b3.FAILURE;
        } else {
            return b3.RUNNING;
        }
    },
    close: function (tick, b3, treeNode) {

    },
    exit: function (tick, b3, treeNode) {

    },

});
