cc.Class({
    extends: cc.Component,

    properties: {

    },


    onLoad: function () {

    },
    enter: function (tick, b3, treeNode) {
        // cc.log("enter Attack");
    },
    open: function (tick, b3, treeNode) {
        // cc.log("open Attack");
    },
    tick: function (tick, b3, treeNode) {
        // cc.log("toAttack");
        if (this.getComponent("Robot").toAttack()){
            this.getComponent("Robot").attackXiaoXueSheng();
            return b3.FAILURE;
        } else {
            return b3.RUNNING;
        }
    },
    close: function (tick, b3, treeNode) {
        // cc.log("close Attack");
    },
    exit: function (tick, b3, treeNode) {
        // cc.log("exit Attack");
    },
});
