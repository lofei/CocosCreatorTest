// lofei
//
//

const { ccclass, property } = cc._decorator;

@ccclass
export default class Robot extends cc.Component {

    @property({ displayName: "HP" })
    mHP: number = 0;

    @property({ displayName: "MP" })
    mMP: number = 0;

    @property({ displayName: "移动速度" })
    mMoveSpeed: number = 0;

    @property({ displayName: "攻击消耗HP" })
    mHPConsume: number = 0;

    @property({ displayName: "攻击消耗MP" })
    mMPConsume: number = 0;

    @property({ displayName: "吃饭恢复HP" })
    mHPRestore: number = 0;

    @property({ displayName: "睡觉恢复MP" })
    mMPRestore: number = 0;

    @property({ type: cc.Node, displayName: "吃饭的地方" })
    mEatPlace: cc.Node = null;

    @property({ type: cc.Node, displayName: "睡觉的地方" })
    mSleepPlace: cc.Node = null;

    @property({ type: cc.Node, displayName: "打小学生的地方" })
    mAttackPlace: cc.Node = null;

    @property({ type: cc.Label, displayName: "提示信息" })
    mTip: cc.Label = null;

    @property({ type: cc.Label, displayName: "状态信息" })
    mStatus: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.mTip.node.active = false;
        cc.log(">> 启动AI");
        this.schedule(function () {
            this.getComponent("BehaviorTreeAttack").tick();
        }, 2);
    }

    update(dt) {
        this.showStatus();
    }

    isEqualPos(pos1, pos2) {
        return Math.round(pos1.x) == Math.round(pos2.x) && Math.round(pos1.y) == Math.round(pos2.y);
    }

    toEat() {
        let toPos = this.mEatPlace.position;
        cc.log(this.node.position + ", " + this.mEatPlace.position);
        if (this.isEqualPos(this.node.position, this.mEatPlace.position)) {
            this.mHP += this.mHPRestore;
            return true;
        } else {
            let self = this;
            this.node.runAction(cc.sequence(
                cc.callFunc(function () {
                    cc.log(">> 饿了，去吃饭");
                }),
                cc.moveTo(this.mMoveSpeed / 1000, toPos),
                cc.callFunc(function () {
                    cc.log(">> 到食堂了，开始吃饭");
                    self.showTips("HP +" + self.mHPRestore);
                })
            ))
        }
    }

    toSleep() {
        let toPos = this.mSleepPlace.position;
        cc.log(this.node.position + ", " + this.mSleepPlace.position);
        if (this.isEqualPos(this.node.position, this.mSleepPlace.position)) {
            this.mMP += this.mMPRestore;
            return true;
        } else {
            let self = this;
            this.node.runAction(cc.sequence(
                cc.callFunc(function () {
                    cc.log(">> 困了，去睡觉");
                }),
                cc.moveTo(this.mMoveSpeed / 1000, toPos),
                cc.callFunc(function () {
                    cc.log(">> 到如家了，开始睡觉");
                    self.showTips("MP +" + self.mMPRestore);
                })
            ))
        }
    }

    toAttack() {
        let toPos = this.mAttackPlace.position;
        cc.log(this.node.position + ", " + this.mAttackPlace.position);
        if (this.isEqualPos(this.node.position, this.mAttackPlace.position)) {
            return true;
        } else {
            this.node.runAction(cc.sequence(
                cc.callFunc(function () {
                    cc.log(">> 无聊了，去网吧打小学生");
                }),
                cc.moveTo(this.mMoveSpeed / 1000, toPos),
                cc.callFunc(function () {
                    cc.log(">> 到网吧了，开始打小学生");
                })
            ));
        }

    }

    attackXiaoXueSheng() {
        if (this.mHP < this.mHPConsume) {
            cc.log(">> 打饿了，去吃饭");
            this.unschedule(this.attackXiaoXueSheng);
            return;
        }

        if (this.mMP < this.mMPConsume) {
            cc.log(">> 打累了，去睡觉");
            this.unschedule(this.attackXiaoXueSheng);
            return;
        }

        cc.log("打了小学生一拳，消耗HP：" + this.mHPConsume + " , 消耗MP： " + this.mMPConsume);
        this.showTips("HP -" + this.mHPConsume + " MP -" + this.mMPConsume);
        this.mHP -= this.mHPConsume;
        this.mMP -= this.mMPConsume;

        this.showStatus();
    }

    showStatus() {
        cc.log("当前状态：\n" + "HP： " + this.mHP + " MP： " + this.mMP);
        this.mStatus.string = "HP： " + this.mHP + " MP： " + this.mMP;
    }

    showTips(tip) {
        this.mTip.node.active = true;
        this.mTip.string = tip;
        this.mTip.node.position = cc.v2(0, 0);
        this.mTip.node.runAction(
            cc.sequence(
                cc.spawn(
                    cc.fadeIn(0.8),
                    cc.moveTo(0.8, cc.v2(0, 100))
                ),
                cc.delayTime(0.2),
                cc.fadeOut(0.5),
            )
        )
    }
}
