// luofei
//

const { ccclass, property } = cc._decorator;

// 打字机动画
@ccclass
export class Typewriter extends cc.Component {

    @property(cc.Button)
    mShowTextBtn: cc.Button = null;

    @property(cc.Label)
    mShowText: cc.Label = null;

    @property(cc.EditBox)
    mInputText: cc.EditBox = null;
    _showing: boolean = false;
    // 进入方法,必须实现
    _intoFunc: any;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this._intoFunc = function () {
            cc.director.loadScene("TypewriterScene");
        }
    }

    start() {

    }

    // update (dt) {},

    // 播放打字动画
    playTypewriteAnimation() {
        if (this._showing) {
            cc.log("正在播放打字动画。");
            return;
        }
        if (this.mInputText.string.length == 0) {
            cc.log("请输入文本");
            return;
        }

        this._showing = true;
        let time = 0;
        let inputText = this.mInputText.string;
        for (let i = 0; i < inputText.length; i++) {
            time = i * 0.05;
            this.showText(inputText.substring(0, i + 1), time, i == inputText.length - 1);
        }
    }

    showText(text, delaytime, isEnd) {
        let self = this;
        this.mShowText.node.runAction(cc.sequence(
            cc.delayTime(delaytime),
            cc.callFunc(function () {
                self.mShowText.string = text;
                if (isEnd) {
                    self._showing = false;
                }
            })
        ));
    }

    backToHome() {
        cc.director.loadScene("TestItemScrollView");
    }
}
