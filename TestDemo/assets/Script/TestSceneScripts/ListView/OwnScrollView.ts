import ListView from "./ListView";

// lofei
//
// 拖尾效果

const {
  ccclass,
  property
} = cc._decorator;

@ccclass
export class OwnScrollView extends cc.ScrollView {

  @property
  isCorecePorTrait: boolean = false;

  // LIFE-CYCLE CALLBACKS:

  _startInertiaScroll(touchMoveVelocity) {
    var inertiaTotalMovement = touchMoveVelocity.mul(1.5);
    this._startAttenuatingAutoScroll(inertiaTotalMovement, touchMoveVelocity);
  }
  _handleMoveLogic(touch) {
    var deltaMove = touch.getDelta();
    if (this.isCorecePorTrait) {
      this._processDeltaMove(cc.v2(deltaMove.y, -deltaMove.x));
    } else {
      this._processDeltaMove(deltaMove);
    }
  },
}