const {ccclass, property} = cc._decorator;

export const enum ItemPos {
    Default,    // 默认
    PreBuff,    // 预缓冲区
    Buff,   // 缓冲区
    View,   // 可视区
}

@ccclass
export default class OwnListItem extends cc.Component {
    @property([cc.Component.EventHandler])
    onEnterScrollBuffArea: cc.Component.EventHandler[] = [];
    @property([cc.Component.EventHandler])
    onExitScrollBuffArea: cc.Component.EventHandler[] = [];
    @property([cc.Component.EventHandler])
    onEnterScrollView: cc.Component.EventHandler[] = [];
    @property([cc.Component.EventHandler])
    onExitScrollView: cc.Component.EventHandler[] = [];
    @property
    debug: boolean = false;

    private _pos: ItemPos = ItemPos.Default;    // item位置
    private _isPreItem: boolean = false;

    enterScrollBuffArea() {
        if (this._pos == ItemPos.Buff) return;
        this._pos = ItemPos.Buff;
        this.onEnterScrollBuffArea.forEach(handle => handle.emit([]));
        // cc.log(this.node['item_index'], 'enterScrollBuffArea')
    }

    exitScrollBuffArea() {
        if (this._pos == ItemPos.Default) return;
        this._pos = ItemPos.Default;
        this.onExitScrollBuffArea.forEach(handle => handle.emit([]));
        // cc.log(this.node['item_index'], 'exitScrollBuffArea')
    }

    enterScrollView() {
        if (this._pos == ItemPos.View) return;
        this._pos = ItemPos.View;
        this.onEnterScrollView.forEach(handle => handle.emit([]));
        // cc.log(this.node['item_index'], 'enterScrollView')
    }

    exitScrollView() {
        if (this._pos == ItemPos.Buff) return;
        this._pos = ItemPos.Buff;
        this.onExitScrollView.forEach(handle => handle.emit([]));
        // cc.log(this.node['item_index'], 'exitScrollView')
    }

    getItemBoundingBox():cc.Rect {
        const box = this.node.getBoundingBoxToWorld();
        const node = this._isPreItem ? this.node.parent : this.node.parent.parent;
        const {x, y} = node.convertToNodeSpaceAR(cc.v2(box.xMin, box.yMin));
        box.x = x;
        box.y = y;
        return box;
    }

    getItemPos(): ItemPos { 
        return this._pos;
    }

    set isPreItem(isPreItem: boolean) {
        this._isPreItem = isPreItem;
    }

    update() {
        if (this.debug) this.drawItem()
    }

    unuse() {
        this._pos = ItemPos.Default;
    }

    reuse() {

    }

    drawItem() {
        let graphic: cc.Graphics = this.node.getComponent(cc.Graphics);
        if (!graphic) {
            this.node.addComponent(cc.Graphics);
            graphic = this.getComponent(cc.Graphics);
        }
        graphic.clear();
        const rect = this.getItemBoundingBox();
        const node = this._isPreItem ? this.node.parent : this.node.parent.parent;
        const nodePos = this.node.convertToNodeSpaceAR(node.convertToWorldSpaceAR(cc.v2(rect.x, rect.y)))
        rect.x = nodePos.x;
        rect.y = nodePos.y;
        graphic.strokeColor = cc.color(0, 0, 0);
        graphic.lineWidth = 10;
        graphic.rect(rect.x, rect.y, rect.width, rect.height);
        graphic.stroke();

        graphic.fillColor = cc.color(255, 0, 0);
        graphic.circle(rect.x, rect.y, 5);
        graphic.fill();

        graphic.fillColor = cc.color(0, 255, 0);
        graphic.circle(rect.xMax, rect.yMax, 5);
        graphic.fill();
    }
}
