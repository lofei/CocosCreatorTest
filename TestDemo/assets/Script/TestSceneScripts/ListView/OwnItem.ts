import OwnListItem from "./OwnListItem";

export class OwnItem extends OwnListItem {
  public updateItemData(data:any) {
      cc.log("Item update content for:", this.dataIndex);
      let text = this.node.getChildByName("text").getComponent(cc.Label);
      text.string = data + this.dataIndex.toString();
  }
}