const {ccclass, property} = cc._decorator;

/**
 * 所有item要实现这个接口
 */
@ccclass
export default abstract class OwnListItem extends cc.Component {
    @property({
        tooltip: 'item类型'
    })
    itemType:string = '';

    dataIndex: number = -1; // item数据索引

    /**
     * @description 更新item数据
     * @param data item数据
     */
    abstract updateItemData(data: any): void;
}
