const {ccclass, property} = cc._decorator;

// eslint-disable-next-line
@ccclass
export default class Item extends cc.Component {
    @property(cc.Label)
    lab: cc.Label = null

    private _removeFromList: Function = null;

    initData(data) {
        this.lab.string = `item-${data.data}`
        // this.lab._updateRenderData(true);
        this._removeFromList = data.removeFromList;
    }

    enterScrollBuffArea() {
        // this.node.opacity = 0;
    }

    exitScrollBuffArea() {
        this._removeFromList && this._removeFromList(this.node);
    }

    enterScrollView() {
        // this.node.opacity = 255;
        // cc.log(this.node.y, this.lab.node.y)
    }

    exitScrollView() {
        // this.node.opacity = 0;
    }

    unuse() {
    }

    reuse() {
        this.node.opacity = 255;
    }
}
