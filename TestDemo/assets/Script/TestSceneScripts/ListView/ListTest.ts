import ListView from "./ListView";
import FramingLoad from "./FramingLoad";
import OwnListView from "./OwnListView";
import { OwnItem } from "./OwnItem";

// lofei
//
// 拖尾效果

const {
    ccclass,
    property
} = cc._decorator;

@ccclass
export class ListTest extends cc.Component {
    @property(OwnListView)
    list: OwnListView = null;
    // @property(ListView)
    // list: ListView = null;
    @property
    itemCount: number = 10;
    @property(cc.Prefab)
    itemPrefab: cc.Prefab = null;

    @property(cc.Node)
    loading: cc.Node = null

    private _listData: any[] = [];
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this._listData = [];
        for (let i = 0; i < 20; ++i) {
            this._listData.push({data: i})
        }
        // this.list.dataListInit(this._listData);
        // this.list.initListView(this.list.node.getContentSize(), () => {
        //     return this._listData;
        // })
        this.list.init({
            getListDataCount: this._getListDataCount,
            getItemSize: this._getItemSize,
            getItemType: this._getItemType,
            getItem: this._getItem,
            getItemData: this._getItemData,
        })
        this.list.reload(false)
    }

    start() {}

    private _getItemData = ():any => {
        return 'data';
    }

    private _getListDataCount = ():number => {
        return this.itemCount;
    }

    private _getItemSize = (index:number):number => {
        if (index % 2 == 0) return 50;
        else return 100;
    }

    private _getItem = ():OwnItem => {
        let node = cc.instantiate(this.itemPrefab);
        return node!.addComponent(OwnItem);
    }

    private _getItemType = ():string => {
        return "OwnItem";
    }
    // onClickAddItem(event, customData) {
    //     this._listData.push({data: this._listData.length});
    //     if (customData == 0) {
    //         this.list.addItemToBottom(this._listData.length - 1);
    //         // this.list._addLastItem(this._listData.length - 1);
    //     } else {
    //         this.list.addItemToTop(this._listData.length - 1);
    //         // this.list._addNextItem(this._listData.length - 1);
    //     }
    // }

    backToHome() {
        cc.director.loadScene("TestItemScrollView");
    }
}