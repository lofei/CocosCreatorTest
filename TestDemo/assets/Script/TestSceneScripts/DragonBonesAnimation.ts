// luofei
//

const { ccclass, property } = cc._decorator;

// 
@ccclass
export class DragonBonesAnimation extends cc.Component {

    // 动画节点
    @property(dragonBones.ArmatureDisplay)
    mAnimationNode: dragonBones.ArmatureDisplay = null;
    // 显示信息
    @property(cc.Label)
    mNowPlayAnimation: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    }

    // update (dt) {},

    onPlayAnimation(eventTouch, playAnimationName:string) {
        if (!playAnimationName || playAnimationName.length < 1) {
            cc.log("未指定播放动画");
            return;
        }

        this.mNowPlayAnimation.string = "正在播放动作：" + playAnimationName;
        this.mAnimationNode.playAnimation(playAnimationName, 0);
    }

    backToHome() {
        cc.director.loadScene("TestItemScrollView");
    }
}
