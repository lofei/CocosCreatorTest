
import { Player } from "./Player";

// lofei
//
const { ccclass, property } = cc._decorator;

/**
 * 
 */
@ccclass
export class WalkingRoute extends cc.Component {
    @property(cc.Prefab)
    mPlayerPrefab:cc.Prefab = null;
    _enemyPool: cc.NodePool;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

        this._enemyPool = new cc.NodePool();
        let initCount = 10;
        for (let i = 0; i < initCount; i++) {
            let p = cc.instantiate(this.mPlayerPrefab);
            this._enemyPool.put(p);
        }

        let self = this;
        setInterval(function(){
            self.createEnemy();
        }, 1000);
        // this.run();
    }

    createEnemy() {
        let enemy = null;
        if (this._enemyPool.size() > 0) {
            enemy = this._enemyPool.get();
        } else {
            enemy = cc.instantiate(this.mPlayerPrefab);
        }
        enemy.parent = this.node;
        enemy.getComponent(Player).run();
    }

    update(dt) {
    }


    backToHome() {
        cc.director.loadScene("TestItemScrollView");
    }
}
